-- connections
-- psql -U airflow -d airflow -h localhost < secret/airflow_connections.sql
INSERT INTO connection
(
  `conn_id`, `conn_type`, `host`, `schema`, `login`, `password`, `port`, `extra`, `is_encrypted`, `is_extra_encrypted`
)
VALUES
  (
    'h3_postgres_prod_read',
    'postgres',
    'ec2-52-206-176-15.compute-1.amazonaws.com',
    'dug0sdrvvebo2',
    'ucfqvbvvdkhre0',
    'paora5b8m0lolae8rs3t6pc5en',
    5432,
    NULL,
    0,
    0
  ),
  (
    'h3_google_cloud_platform_preprod',
    'google_cloud_platform',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    '{"extra__google_cloud_platform__scope": "https://www.googleapis.com/auth/devstorage.read_write,https://www.googleapis.com/auth/bigquery,https://www.googleapis.com/auth/analytics.readonly", "extra__jdbc__drv_path": "", "extra__google_cloud_platform__project": "iconic-being-134220", "extra__google_cloud_platform__key_path": "secret/airflow_google_h3_preprod.json", "extra__jdbc__drv_clsname": ""}',
    0,
    0
  ),
  (
    'h3_google_cloud_platform_data',
    'google_cloud_platform',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    '{"extra__google_cloud_platform__scope": "https://www.googleapis.com/auth/devstorage.read_write,https://www.googleapis.com/auth/bigquery,https://www.googleapis.com/auth/analytics.readonly", "extra__jdbc__drv_path": "", "extra__google_cloud_platform__project": "h3-data", "extra__google_cloud_platform__key_path": "secret/airflow_google_h3_data.json", "extra__jdbc__drv_clsname": ""}',
    0,
    0
  ),
  (
    'h3_s3_prod_read',
    'aws',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    '{"region_name": "us-east-1", "aws_secret_access_key": "qEuuYB8fNxPc4FZvGJc65MnFoXkwzLQMIVb1TRpa", "aws_access_key_id": "AKIAJTQO2KMZRVEWJYXA"}',
    0,
    0
  );
