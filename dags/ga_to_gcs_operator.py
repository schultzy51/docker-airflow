# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import time
import logging

from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from tempfile import NamedTemporaryFile
from datetime import datetime

from ga_hook import GoogleAnalyticsHook


class GoogleAnalyticsToGoogleCloudStorageOperator(BaseOperator):
    """
    Copy data from Google Analytics to Google cloud storage in JSON format.
    """
    template_fields = ('request', 'bucket', 'filename')
    ui_color = '#a0e08c'

    @apply_defaults
    def __init__(self,
                 request,
                 bucket,
                 filename,
                 approx_max_file_size_bytes=1900000000,
                 google_analytics_conn_id='google_analytics_default',
                 google_cloud_storage_conn_id='google_cloud_storage_default',
                 delegate_to=None,
                 *args,
                 **kwargs):
        """
        :param request: The report being requested.
        :type request: string
        :param bucket: The bucket to upload to.
        :type bucket: string
        :param filename: The filename to use as the object name when uploading
            to Google cloud storage. A {} should be specified in the filename
            to allow the operator to inject file numbers in cases where the
            file is split due to size.
        :type filename: string
        :param approx_max_file_size_bytes: This operator supports the ability
            to split large table dumps into multiple files (see notes in the
            filenamed param docs above). Google cloud storage allows for files
            to be a maximum of 4GB. This param allows developers to specify the
            file size of the splits.
        :type approx_max_file_size_bytes: long
        :param google_analytics_conn_id: Reference to a specific GoogleAnalytics hook.
        :type google_analytics_conn_id: string
        :param google_cloud_storage_conn_id: Reference to a specific Google
            cloud storage hook.
        :type google_cloud_storage_conn_id: string
        :param delegate_to: The account to impersonate, if any. For this to
            work, the service account making the request must have domain-wide
            delegation enabled.
        """
        super(GoogleAnalyticsToGoogleCloudStorageOperator, self).__init__(*args, **kwargs)
        self.request = request;
        self.bucket = bucket
        self.filename = filename
        self.approx_max_file_size_bytes = approx_max_file_size_bytes
        self.google_analytics_conn_id = google_analytics_conn_id
        self.google_cloud_storage_conn_id = google_cloud_storage_conn_id
        self.delegate_to = delegate_to

    def execute(self, context):
        rows = self._query_google_analytics()
        files_to_upload = self._write_local_data_files(rows)

        # Flush all files before uploading
        for file_handle in files_to_upload.values():
            file_handle.flush()

        self._upload_to_gcs(files_to_upload)

        # Close all temp file handles.
        for file_handle in files_to_upload.values():
            file_handle.close()

    def _query_google_analytics(self):
        """
        Queries postgres and returns a cursor to the results.
        """
        google_analytics = GoogleAnalyticsHook(google_analytics_conn_id=self.google_analytics_conn_id)
        response = google_analytics.get_reports(self.request)
        rows = self._convert_response_to_rows(response)
        return rows

    def _convert_response_to_rows(self, response):
        dict_rows = []
        now = time.mktime(datetime.today().timetuple())

        reports = response.get('reports', [])
        if len(reports) > 0:
            # Only use the first report
            report= reports[0]

            column_header = report.get('columnHeader', {})
            dimension_headers = column_header.get('dimensions', [])
            metric_headers = column_header.get('metricHeader', {}).get('metricHeaderEntries', [])
            rows = report.get('data', {}).get('rows', [])

            for row in rows:
                dimensions = row.get('dimensions', [])
                date_range_values = row.get('metrics', [])
                obj = {}
                obj['date'] = now

                for header, dimension in zip(dimension_headers, dimensions):
                    obj[header.replace(':', '_')] = dimension

                obj['ranges'] = []
                for i, values in enumerate(date_range_values):
                    date_range_obj = {"range": i}

                    for metricHeader, value in zip(metric_headers, values.get('values')):
                        date_range_obj[metricHeader.get('name').replace(':', '_')] = value

                    obj['ranges'].append(date_range_obj)

                dict_rows.append(obj)

        return dict_rows

    def _write_local_data_files(self, rows):
        """
        Takes rows of data and writes to a local file.

        :return: A dictionary where keys are filenames to be used as object
            names in GCS, and values are file handles to local files that
            contain the data for the GCS objects.
        """
        file_no = 0
        tmp_file_handle = NamedTemporaryFile(delete=True)
        tmp_file_handles = {self.filename.format(file_no): tmp_file_handle}

        for row_dict in rows:
            # TODO validate that row isn't > 2MB. BQ enforces a hard row size of 2MB.
            json.dump(row_dict, tmp_file_handle)

            # Append newline to make dumps BigQuery compatible.
            tmp_file_handle.write('\n')

            # Stop if the file exceeds the file size limit.
            if tmp_file_handle.tell() >= self.approx_max_file_size_bytes:
                file_no += 1
                tmp_file_handle = NamedTemporaryFile(delete=True)
                tmp_file_handles[self.filename.format(file_no)] = tmp_file_handle

        return tmp_file_handles

    def _upload_to_gcs(self, files_to_upload):
        """
        Upload all of the file splits (and optionally the schema .json file) to
        Google cloud storage.
        """
        hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=self.google_cloud_storage_conn_id,
                                      delegate_to=self.delegate_to)
        for object, tmp_file_handle in files_to_upload.items():
            hook.upload(self.bucket, object, tmp_file_handle.name, 'application/json')
