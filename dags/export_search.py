from airflow import DAG
from airflow.operators import PythonOperator
from airflow.operators import DummyOperator
from airflow.hooks import PostgresHook
from airflow.hooks import S3Hook
from datetime import datetime, timedelta
from decimal import Decimal
from datetime import date, datetime
from tempfile import NamedTemporaryFile
import logging
import json
import time

from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'export_search'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

dag = DAG(TASK_BASENAME, schedule_interval='0 5 * * *', default_args=default_args)

POSTGRES_QUERY = """SELECT
                      user_id,
                      position_id,
                      employer_id,
                      uuid,
                      created_at,
                      updated_at,
                      impersonator
                    FROM
                      searches
                    WHERE
                      created_at < (NOW() - INTERVAL '12 HOURS') AND
                      created_at > '2015-07-29' LIMIT 100"""
BQ_QUERY = """SELECT
                search_uuid
              FROM {}.search_logs""".format(constants.BQ_DATASET_NAME)
BQ_SCHEMA = [
    {"name": "search_uuid", "type": "STRING", "mode": "NULLABLE"},
    {"name": "total", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "page_size", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "page", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "profiles", "type": "INTEGER", "mode": "REPEATED"},
    {"name": "search_criteria",
     "type": "RECORD",
     "mode": "NULLABLE",
     "fields":
         [{"name": "categories", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "category_functions", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "sectors", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "project_types", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "primary_specialties", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "secondary_specialties", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "min_experience", "type": "STRING", "mode": "NULLABLE"},
          {"name": "max_experience", "type": "STRING", "mode": "NULLABLE"},
          {"name": "min_salary_preference", "type": "STRING", "mode": "NULLABLE"},
          {"name": "max_salary_preference", "type": "STRING", "mode": "NULLABLE"},
          {"name": "travel_preferences", "type": "STRING", "mode": "REPEATED"},
          {"name": "locations", "type": "STRING", "mode": "REPEATED"},
          {"name": "must_have_resumes", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "must_have_projects", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "must_be_consultant", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "must_be_veteran", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "must_be_skipped", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "exclude_needs_sponsorship", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "exclude_willing_to_work", "type": "BOOLEAN", "mode": "NULLABLE"},
          {"name": "sort", "type": "STRING", "mode": "NULLABLE"},
          {"name": "page", "type": "STRING", "mode": "NULLABLE"},
          {"name": "salary_preferences", "type": "STRING", "mode": "REPEATED"},
          {"name": "experiences", "type": "STRING", "mode": "REPEATED"},
          {"name": "sub_project_types", "type": "STRING", "mode": "REPEATED"},
          {"name": "locked_filter", "type": "STRING", "mode": "NULLABLE"}]},
    {"name": "elasticsearch_json", "type": "STRING", "mode": "NULLABLE"},
    {"name": "employer_criteria",
     "type": "RECORD",
     "mode": "NULLABLE",
     "fields":
         [{"name": "employer_id", "type": "INTEGER", "mode": "NULLABLE"},
          {"name": "employer_name", "type": "STRING", "mode": "NULLABLE"},
          {"name": "categories", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "category_functions", "type": "INTEGER", "mode": "REPEATED"},
          {"name": "project_types", "type": "STRING", "mode": "REPEATED"},
          {"name": "sectors", "type": "INTEGER", "mode": "REPEATED"}]},
    {"name": "user_id", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "position_id", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "uuid", "type": "STRING", "mode": "NULLABLE"},
    {"name": "created_at", "type": "TIMESTAMP", "mode": "NULLABLE"},
    {"name": "updated_at", "type": "TIMESTAMP", "mode": "NULLABLE"},
    {"name": "impersonator", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "id", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "employer_id", "type": "INTEGER", "mode": "NULLABLE"}
]

GCS_FILENAME ="export_searches_backlog.json"

def get_potential_migrations(postgres_hook):
    conn = postgres_hook.get_conn()
    cursor = conn.cursor()
    cursor.execute(POSTGRES_QUERY)
    schema = map(lambda schema_tuple: schema_tuple[0], cursor.description)
    rows = cursor.fetchall()
    logging.info("Found {} postgres rows!".format(len(rows)))

    postgres_row_dicts = []
    for row in rows:
        row_dict = dict(zip(schema, row))
        postgres_row_dicts.append(row_dict)

    return postgres_row_dicts


def get_already_migrated_uuids(bg_hook):
    conn = bg_hook.get_conn()
    cursor = conn.cursor()
    cursor.execute(BQ_QUERY)
    rows = cursor.fetchall()
    logging.info("Found {} bigquery rows!".format(len(rows)))

    exclude_uuids = [row[0] for row in rows]

    return exclude_uuids


def write_local_data_files(rows):
    file_no = 0
    filename="{}/{}".format(TASK_BASENAME, GCS_FILENAME)
    tmp_file_handle = NamedTemporaryFile(delete=True)
    tmp_file_handles = {filename.format(file_no): tmp_file_handle}

    for row_dict in rows:
        # Convert datetime objects to utc seconds, and decimals to floats
        row_dict = {k: convert_types(v) for k, v in row_dict.items()}

        json.dump(row_dict, tmp_file_handle)

        # Append newline to make dumps BigQuery compatible.
        tmp_file_handle.write('\n')

        # Stop if the file exceeds the file size limit.
        if tmp_file_handle.tell() >= 1900000000:
            file_no += 1
            tmp_file_handle = NamedTemporaryFile(delete=True)
            tmp_file_handles[filename.format(file_no)] = tmp_file_handle

    return tmp_file_handles


def upload_to_gcs(files_to_upload):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=constants.GCP_CONN_ID)
    for object, tmp_file_handle in files_to_upload.items():
        hook.upload(constants.GCS_BUCKET_NAME, object, tmp_file_handle.name, 'application/json')


def convert_types(value):
    if type(value) in (datetime, date):
        return time.mktime(value.timetuple())
    elif isinstance(value, Decimal):
        return float(value)
    else:
        return value


def create_dict(s3, row):
    result = None

    # check if the search has a uuid
    if row['uuid']:
        key = "searches/{}/search_{}.json".format(row['created_at'].strftime("%Y-%m-%d"), row['uuid'])
        exists = s3.check_for_key(key, bucket_name=constants.S3_BUCKET_NAME)
        logging.info("Key: {}, S3 Key found: {}".format(key, exists))

        # only create the row if the json exists
        if exists:
            s3_key_object = s3.get_key(key, bucket_name=constants.S3_BUCKET_NAME)
            contents_json = s3_key_object.get_contents_as_string()
            contents_dict = json.loads(contents_json)

            result = dict(contents_dict, **row)

    return result


def migrate(**kwargs):
    s3_hook = S3Hook(s3_conn_id=constants.S3_CONN_ID)
    bg_hook = BigQueryHook(bigquery_conn_id=constants.GCP_CONN_ID)
    postgres_hook = PostgresHook(postgres_conn_id=constants.POSTGRES_CONN_ID)

    rows = get_potential_migrations(postgres_hook)

    exclude_uuids = get_already_migrated_uuids(bg_hook)
    logging.info(exclude_uuids)

    valid_rows = []

    for row in rows:
        if row['uuid'] not in exclude_uuids:
            content_dict = create_dict(s3_hook, row)
            logging.info(content_dict)
            if content_dict:
                valid_rows.append(content_dict)

    files_to_upload = write_local_data_files(valid_rows)

    # Flush all files before uploading
    for file_handle in files_to_upload.values():
        file_handle.flush()

    upload_to_gcs(files_to_upload)

    # Close all temp file handles.
    for file_handle in files_to_upload.values():
        file_handle.close()

    return 'Done!'

bq_create_op = GoogleCloudStorageToBigQueryOperator(
    bucket=constants.GCS_BUCKET_NAME,
    source_objects=[constants.GCS_EMPTY_JSON],
    destination_project_dataset_table="{}.search_logs".format(constants.BQ_DATASET_NAME),
    schema_fields=BQ_SCHEMA,
    source_format='NEWLINE_DELIMITED_JSON',
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    task_id="{}_bq_create".format(TASK_BASENAME),
    dag=dag
)

search = PythonOperator(
    task_id="{}_python".format(TASK_BASENAME),
    python_callable=migrate,
    dag=dag
)

search.set_upstream(bq_create_op)

gcs_to_bq_op = GoogleCloudStorageToBigQueryOperator(
    bucket=constants.GCS_BUCKET_NAME,
    source_objects=["{}/{}".format(TASK_BASENAME, GCS_FILENAME)],
    destination_project_dataset_table="{}.search_logs".format(constants.BQ_DATASET_NAME),
    schema_fields=BQ_SCHEMA,
    source_format='NEWLINE_DELIMITED_JSON',
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    task_id="{}_bq".format(TASK_BASENAME),
    dag=dag
)

gcs_to_bq_op.set_upstream(search)
