# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import time

from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from datetime import date, datetime
from decimal import Decimal
from tempfile import NamedTemporaryFile


class PostgresToGoogleCloudStorageOperator(BaseOperator):
    """
    Copy data from Postgres to Google cloud storage in JSON format.
    """
    template_fields = ('sql', 'bucket', 'filename', 'schema_filename')
    template_ext = ('.sql',)
    ui_color = '#a0e08c'

    @apply_defaults
    def __init__(self,
                 sql,
                 bucket,
                 filename,
                 schema_filename=None,
                 approx_max_file_size_bytes=1900000000,
                 postgres_conn_id='postgres_default',
                 google_cloud_storage_conn_id='google_cloud_storage_default',
                 delegate_to=None,
                 *args,
                 **kwargs):
        """
        :param sql: The SQL to execute on the Postgres table.
        :type sql: string
        :param bucket: The bucket to upload to.
        :type bucket: string
        :param filename: The filename to use as the object name when uploading
            to Google cloud storage. A {} should be specified in the filename
            to allow the operator to inject file numbers in cases where the
            file is split due to size.
        :type filename: string
        :param schema_filename: If set, the filename to use as the object name
            when uploading a .json file containing the BigQuery schema fields
            for the table that was dumped from Postgres.
        :type schema_filename: string
        :param approx_max_file_size_bytes: This operator supports the ability
            to split large table dumps into multiple files (see notes in the
            filenamed param docs above). Google cloud storage allows for files
            to be a maximum of 4GB. This param allows developers to specify the
        file size of the splits.
        :type approx_max_file_size_bytes: long
        :param postgres_conn_id: Reference to a specific Postgres hook.
        :type postgres_conn_id: string
        :param google_cloud_storage_conn_id: Reference to a specific Google
            cloud storage hook.
        :type google_cloud_storage_conn_id: string
        :param delegate_to: The account to impersonate, if any. For this to
            work, the service account making the request must have domain-wide
            delegation enabled.
        """
        super(PostgresToGoogleCloudStorageOperator, self).__init__(*args, **kwargs)
        self.sql = sql;
        self.bucket = bucket
        self.filename = filename
        self.schema_filename = schema_filename
        self.approx_max_file_size_bytes = approx_max_file_size_bytes
        self.postgres_conn_id = postgres_conn_id
        self.google_cloud_storage_conn_id = google_cloud_storage_conn_id
        self.delegate_to = delegate_to

    def execute(self, context):
        cursor = self._query_postgres()
        files_to_upload = self._write_local_data_files(cursor)

        # If a schema is set, create a BQ schema JSON file.
        if self.schema_filename:
            files_to_upload.update(self._write_local_schema_file(cursor))

        # Flush all files before uploading
        for file_handle in files_to_upload.values():
            file_handle.flush()

        self._upload_to_gcs(files_to_upload)

        # Close all temp file handles.
        for file_handle in files_to_upload.values():
            file_handle.close()

    def _query_postgres(self):
        """
        Queries postgres and returns a cursor to the results.
        """
        postgres = PostgresHook(postgres_conn_id=self.postgres_conn_id)
        conn = postgres.get_conn()
        cursor = conn.cursor()
        cursor.execute(self.sql)
        return cursor

    def _write_local_data_files(self, cursor):
        """
        Takes a cursor, and writes results to a local file.

        :return: A dictionary where keys are filenames to be used as object
            names in GCS, and values are file handles to local files that
            contain the data for the GCS objects.
        """
        schema = map(lambda schema_tuple: schema_tuple[0], cursor.description)
        file_no = 0
        tmp_file_handle = NamedTemporaryFile(delete=True)
        tmp_file_handles = {self.filename.format(file_no): tmp_file_handle}

        for row in cursor:
            # TODO: determine if this is necessary for postgres
            # Convert datetime objects to utc seconds, and decimals to floats
            row = map(self.convert_types, row)
            row_dict = dict(zip(schema, row))

            # TODO validate that row isn't > 2MB. BQ enforces a hard row size of 2MB.
            json.dump(row_dict, tmp_file_handle)

            # Append newline to make dumps BigQuery compatible.
            tmp_file_handle.write('\n')

            # Stop if the file exceeds the file size limit.
            if tmp_file_handle.tell() >= self.approx_max_file_size_bytes:
                file_no += 1
                tmp_file_handle = NamedTemporaryFile(delete=True)
                tmp_file_handles[self.filename.format(file_no)] = tmp_file_handle

        return tmp_file_handles

    def _write_local_schema_file(self, cursor):
        """
        Takes a cursor, and writes the BigQuery schema for the results to a
        local file system.

        :return: A dictionary where key is a filename to be used as an object
            name in GCS, and values are file handles to local files that
            contains the BigQuery schema fields in .json format.
        """
        schema = []
        for field in cursor.description:
            # See PEP 249 for details about the description tuple.
            field_name = field[0]
            field_type = self.type_map(field[1])
            # Always allow TIMESTAMP to be nullable. MySQLdb returns None types
            # for required fields because some MySQL timestamps can't be
            # represented by Python's datetime (e.g. 0000-00-00 00:00:00).
            # TODO: determine if this is necessary for postgres
            field_mode = 'NULLABLE' if field[6] or field_type == 'TIMESTAMP' else 'REQUIRED'
            schema.append({
                'name': field_name,
                'type': field_type,
                'mode': field_mode,
            })

        logging.info('Using schema for %s: %s', self.schema_filename, schema)
        tmp_schema_file_handle = NamedTemporaryFile(delete=True)
        json.dump(schema, tmp_schema_file_handle)
        return {self.schema_filename: tmp_schema_file_handle}

    def _upload_to_gcs(self, files_to_upload):
        """
        Upload all of the file splits (and optionally the schema .json file) to
        Google cloud storage.
        """
        hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=self.google_cloud_storage_conn_id,
                                      delegate_to=self.delegate_to)
        for object, tmp_file_handle in files_to_upload.items():
            hook.upload(self.bucket, object, tmp_file_handle.name, 'application/json')

    @classmethod
    def convert_types(cls, value):
        """
        Takes a value from psycopg2, and converts it to a value that's safe for
        JSON/Google cloud storage/BigQuery. Dates are converted to UTC seconds.
        Decimals are converted to floats.
        """
        # logging.info('convert_types: %s', value)
        # TODO: determine if this is necessary for postgres
        if type(value) in (datetime, date):
            return time.mktime(value.timetuple())
        elif isinstance(value, Decimal):
            return float(value)
        else:
            return value

    @classmethod
    def type_map(cls, postgres_type):
        """
        Helper function that maps from Postgres fields to BigQuery fields. Used
        when a schema_filename is set.
        """
        # TODO: verify if any of these are wrong or missing
        d = {
            16: 'BOOLEAN',  # BOOLOID
            # 17: '',  # BYTEAOID
            # 18: '',  # CHAROID
            # 19: '',  # NAMEOID
            20: 'INTEGER',  # INT8OID
            21: 'INTEGER',  # INT2OID
            # 22: '',  # INT2VECTOROID
            23: 'INTEGER',  # INT4OID
            # 24: '',  # REGPROCOID
            # 25: '',  # TEXTOID
            # 26: '',  # OIDOID
            # 27: '',  # TIDOID
            # 28: '',  # XIDOID
            # 29: '',  # CIDOID
            # 30: '',  # OIDVECTOROID
            # 71: '',  # PG_TYPE_RELTYPE_OID
            # 75: '',  # PG_ATTRIBUTE_RELTYPE_OID
            # 81: '',  # PG_PROC_RELTYPE_OID
            # 83: '',  # PG_CLASS_RELTYPE_OID
            # 600: '',  # POINTOID
            # 601: '',  # LSEGOID
            # 602: '',  # PATHOID
            # 603: '',  # BOXOID
            # 604: '',  # POLYGONOID
            # 628: '',  # LINEOID
            700: 'FLOAT',  # FLOAT4OID
            701: 'FLOAT',  # FLOAT8OID
            702: 'TIMESTAMP',  # ABSTIMEOID
            703: 'TIMESTAMP',  # RELTIMEOID
            # 704: '',  # TINTERVALOID
            # 705: '',  # UNKNOWNOID
            # 718: '',  # CIRCLEOID
            # 790: '',  # CASHOID
            # 829: '',  # MACADDROID
            # 869: '',  # INETOID
            # 650: '',  # CIDROID
            # 1007: '',  # INT4ARRAYOID
            # 1033: '',  # ACLITEMOID
            # 1042: '',  # BPCHAROID
            # 1043: '',  # VARCHAROID
            1082: 'TIMESTAMP',  # DATEOID
            1083: 'TIMESTAMP',  # TIMEOID
            1114: 'TIMESTAMP',  # TIMESTAMPOID
            1184: 'TIMESTAMP',  # TIMESTAMPTZOID
            # 1186: '',  # INTERVALOID
            # 1266: '',  # TIMETZOID
            1560: 'INTEGER',  # BITOID
            1562: 'INTEGER',  # VARBITOID
            1700: 'FLOAT',  # NUMERICOID
            # 1790: '',  # REFCURSOROID
            # 2202: '',  # REGPROCEDUREOID
            # 2203: '',  # REGOPEROID
            # 2204: '',  # REGOPERATOROID
            # 2205: '',  # REGCLASSOID
            # 2206: '',  # REGTYPEOID
            # 2249: '',  # RECORDOID
            # 2275: '',  # CSTRINGOID
            # 2276: '',  # ANYOID
            # 2277: '',  # ANYARRAYOID
            # 2278: '',  # VOIDOID
            # 2279: '',  # TRIGGEROID
            # 2280: '',  # LANGUAGE_HANDLEROID
            # 2281: '',  # INTERNALOID
            # 2282: '',  # OPAQUEOID
            # 2283: ''  # ANYELEMENTOID
        }
        return d[postgres_type] if postgres_type in d else 'STRING'
