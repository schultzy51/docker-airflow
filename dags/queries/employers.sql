SELECT
  "id",
  "name",
  "created_at",
  "updated_at",
  "wizard_completed_at",
  "hidden_at",
  "brand_score"
FROM
  "employers";
