SELECT
  "id",
  "token_digest",
  "retrieved_at",
  "created_at",
  "updated_at"
FROM
  "reverse_sync_tokens";
