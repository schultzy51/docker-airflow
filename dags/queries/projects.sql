SELECT
  "id",
  "project_value",
  "start_date",
  "end_date",
  "client",
  "project_name",
  "description",
  "project_type_id",
  "professional_profile_id",
  "category_function_id",
  "created_at",
  "updated_at",
  "current"
FROM
  "projects";
