SELECT
  "id",
  "trade_association_id",
  "employer_id",
  "association_code",
  "created_at",
  "updated_at"
FROM
  "employers_trade_associations";
