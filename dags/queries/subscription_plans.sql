SELECT
  "id",
  "braintree_plan_id",
  "plan_type",
  "number_of_recurring_credits",
  "description",
  "terminated_at",
  "created_at",
  "updated_at",
  "last_checked_at",
  "last_status",
  "price_cents",
  "currency"
FROM
  "subscription_plans";
