SELECT
  "id",
  "name",
  "hubspot_assisted_search_survey_key",
  "employer_id",
  "created_at",
  "updated_at"
FROM
  "assisted_search_surveys";
