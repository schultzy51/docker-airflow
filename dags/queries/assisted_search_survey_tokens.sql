SELECT
  "id",
  "token",
  "professional_profile_id",
  "assisted_search_survey_id",
  "created_at",
  "updated_at"
FROM
  "assisted_search_survey_tokens";
