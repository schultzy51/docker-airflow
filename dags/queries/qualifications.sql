SELECT
  "id",
  "professional_profile_id",
  "additional_skills",
  "additional_certs",
  "created_at",
  "updated_at"
FROM
  "qualifications";
