SELECT
  "id",
  "professional_profile_id",
  "share_with",
  "created_at",
  "updated_at"
FROM
  "sharing_preferences";
