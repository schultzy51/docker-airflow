import logging
import csv
import os

from airflow import DAG
from airflow.operators import PythonOperator
from airflow.operators import EmailOperator
from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.contrib.operators.gcs_download_operator import GoogleCloudStorageDownloadOperator
from datetime import datetime, timedelta, date
from tempfile import NamedTemporaryFile

# custom
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'summary_csv'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

# Once a week on Sunday
dag = DAG(TASK_BASENAME, schedule_interval='0 9 * * 0', default_args=default_args)

BQ_POSITIONS_CREATED_QUERY = """
    SELECT
        DATE(created_at) AS day,
        COUNT(*) AS positions_created
    FROM {}.positions AS p
    WHERE p.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -8, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

BQ_POSITIONS_TOTAL_QUERY = """
    SELECT
        DATE(DATE_ADD(CURRENT_DATE(), -1, "DAY")) AS day,
        SUM(IF(p.terminated_at IS NULL, 1, 0)) AS positions_active_total
    FROM
        {}.positions AS p
    WHERE
        p.created_at < TIMESTAMP(CURRENT_DATE())
""".format(constants.BQ_DATASET_NAME)

BQ_SEARCHES_PERFORMED_QUERY = """
    SELECT
        DATE(created_at) AS day,
        COUNT(*) AS searches_performed
    FROM {}.searches AS s
    WHERE s.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -8, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

BQ_EMPLOYER_SIGNUPS_CREATED_QUERY = """
    SELECT
        DATE(e.created_at) AS day,
        COUNT(*) AS employer_signups
    FROM {}.employers AS e
    WHERE e.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -8, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

BQ_EMPLOYER_SIGNUPS_TOTAL_QUERY = """
    SELECT
        DATE(DATE_ADD(CURRENT_DATE(), -1, "DAY")) AS day,
        COUNT(*) AS employer_signups_total
    FROM
        {}.employers AS e
    WHERE
        e.created_at < TIMESTAMP(CURRENT_DATE())
""".format(constants.BQ_DATASET_NAME)

BQ_PROFILE_EMPLOYER_RATING_QUERY = """
    SELECT
        DATE(r.created_at) AS day,
        SUM(IF(r.wants_to_work_here IS TRUE, 1, 0)) AS thumbs_up,
        SUM(IF(r.wants_to_work_here IS FALSE, 1, 0)) AS thumbs_down,
        COUNT(*) AS thumbs
    FROM {}.profile_employer_ratings AS r
    WHERE r.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -8, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

BQ_PROFILE_EMPLOYER_RATING_TOTAL_QUERY = """
    SELECT
        DATE(DATE_ADD(CURRENT_DATE(), -1, "DAY")) AS day,
        SUM(IF(r.wants_to_work_here IS TRUE, 1, 0)) AS thumbs_up,
        SUM(IF(r.wants_to_work_here IS FALSE, 1, 0)) AS thumbs_down,
        COUNT(*) AS thumbs
    FROM
        {}.profile_employer_ratings AS r
    WHERE
        r.created_at < TIMESTAMP(CURRENT_DATE())
""".format(constants.BQ_DATASET_NAME)

BQ_JOBSEEKER_SIGNUPS_QUERY = """
    SELECT
        DATE(p.created_at) AS day,
        COUNT(*) AS wizards_started,
        SUM(IF(p.wizard_completed_at IS NOT NULL, 1, 0)) AS wizard_completed
    FROM {}.professional_profiles AS p
    WHERE p.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -8, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

BQ_JOBSEEKER_SIGNUPS_TOTAL_QUERY = """
    SELECT
        DATE(DATE_ADD(CURRENT_DATE(), -1, "DAY")) AS day,
        COUNT(*) AS wizards_started,
        SUM(IF(p.wizard_completed_at IS NOT NULL, 1, 0)) AS wizard_completed
    FROM
        {}.professional_profiles AS p
    WHERE
        p.created_at < TIMESTAMP(CURRENT_DATE())
""".format(constants.BQ_DATASET_NAME)

BQ_COMPANY_PROFILE_VIEWS_QUERY = """
    SELECT
        DATE(s.date) AS day,
        SUM(INTEGER(s.ranges.ga_pageviews)) as total
    FROM {}.summary_pageviews_per_company_profile AS s
    WHERE s.date > DATE_ADD(CURRENT_TIMESTAMP(), -2, 'DAY')
    GROUP BY day
    ORDER BY day;
""".format(constants.BQ_DATASET_NAME)

##################################################################

GCS_FILENAME = 'summary.csv'


def _write_local_data_files(rows):
    filename = "{}/{}".format(TASK_BASENAME, GCS_FILENAME)

    tmp_file_handle = NamedTemporaryFile(delete=True)
    tmp_file_handles = {filename: tmp_file_handle}

    writer = csv.writer(tmp_file_handle, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerows(rows)

    return tmp_file_handles


def _upload_to_gcs(files_to_upload):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=constants.GCP_CONN_ID,
                                  delegate_to=None)
    for object, tmp_file_handle in files_to_upload.items():
        hook.upload(constants.GCS_BUCKET_NAME, object, tmp_file_handle.name, 'application/json')


def run_query(bql):
    hook = BigQueryHook(bigquery_conn_id=constants.GCP_CONN_ID, delegate_to=None)
    conn = hook.get_conn()
    cursor = conn.cursor()
    cursor.execute(bql)
    return cursor.fetchall()


def convert_results(query_type, query_name, date_range, results, item_index=0, cumulative=False, weekly=False):
    # each row needs to be of the format [date, count, count, ...]
    # date should be first in the results array of arrays
    converted = []
    results_dict = {}

    for result in results:
        results_dict[datetime.strptime(result[0], '%Y-%m-%d').date()] = result[1:]

    if weekly:
        filler = [''] * (len(date_range))
        items = results_dict.get(date_range[-1], None)
        item = items[item_index] if items else 0
        converted = [query_type, query_name] + filler + [item]
    else:
        prev_item = 0
        for day in date_range:
            items = results_dict.get(day, None)
            if items:
                prev_item = items[item_index]
                converted.append(items[item_index])
            else:
                converted.append(prev_item if cumulative else 0)

        total = (converted[-1] - converted[0]) if cumulative else sum(converted)
        converted.append(total)

        converted = [query_type, query_name] + converted

    return converted


def gather_data():
    rows = []

    now = datetime.now()
    today = now.date()
    date_range = []

    # previous 7 dates
    for i in range(7, 0, -1):
        prev = today - timedelta(days=i)
        date_range.append(prev)

    # headers
    rows.append([now.strftime('%Y%m%d%H%M%S'), ''] + [day.strftime('%a') for day in date_range])
    formatted_dates = [day.strftime('%m/%d') for day in date_range]
    rows.append(['', ''] + formatted_dates + ["{} - {}".format(formatted_dates[0], formatted_dates[-1])])

    # query and results
    results = run_query(BQ_EMPLOYER_SIGNUPS_CREATED_QUERY)
    rows.append(convert_results('Employer', 'Accounts Created', date_range, results))

    results = run_query(BQ_EMPLOYER_SIGNUPS_TOTAL_QUERY)
    rows.append(convert_results('Employer', 'Accounts Total', date_range, results, weekly=True))

    results = run_query(BQ_POSITIONS_CREATED_QUERY)
    rows.append(convert_results('Employer', 'Positions Created', date_range, results))

    results = run_query(BQ_POSITIONS_TOTAL_QUERY)
    rows.append(convert_results('Employer', 'Positions Active Total', date_range, results, weekly=True))

    results = run_query(BQ_SEARCHES_PERFORMED_QUERY)
    rows.append(convert_results('Employer', 'Searches Performed', date_range, results))

    results = run_query(BQ_COMPANY_PROFILE_VIEWS_QUERY)
    rows.append(convert_results('Employer', 'Company Profile Views', date_range, results, weekly=True))

    results = run_query(BQ_PROFILE_EMPLOYER_RATING_QUERY)
    rows.append(convert_results('Employer', 'Thumbs Up', date_range, results, item_index=0))
    rows.append(convert_results('Employer', 'Thumbs Down', date_range, results, item_index=1))
    rows.append(convert_results('Employer', 'Thumbs Up/Down', date_range, results, item_index=2))

    results = run_query(BQ_PROFILE_EMPLOYER_RATING_TOTAL_QUERY)
    rows.append(convert_results('Employer', 'Thumbs Up Total', date_range, results, item_index=0, weekly=True))
    rows.append(convert_results('Employer', 'Thumbs Down Total', date_range, results, item_index=1, weekly=True))
    rows.append(
        convert_results('Employer', 'Thumbs Up/Down Total', date_range, results, item_index=2, weekly=True))

    results = run_query(BQ_JOBSEEKER_SIGNUPS_QUERY)
    rows.append(convert_results('JobSeeker', 'Accounts Created', date_range, results, item_index=0))
    rows.append(convert_results('JobSeeker', 'Wizards Completed', date_range, results, item_index=1))

    results = run_query(BQ_JOBSEEKER_SIGNUPS_TOTAL_QUERY)
    rows.append(convert_results('JobSeeker', 'Accounts Total', date_range, results, item_index=0, weekly=True))
    rows.append(
        convert_results('JobSeeker', 'Wizards Completed Total', date_range, results, item_index=1, weekly=True))

    return rows


def summary_csv():
    logging.info('Creating Summary CSV')

    rows = gather_data()

    files_to_upload = _write_local_data_files(rows)

    for file_handle in files_to_upload.values():
        file_handle.flush()

    _upload_to_gcs(files_to_upload)

    for file_handle in files_to_upload.values():
        file_handle.close()

    return rows


elasticsearch_to_gcs_op = PythonOperator(
    task_id="{}_python".format(TASK_BASENAME),
    python_callable=summary_csv,
    dag=dag
)

LOCAL_FILENAME = '/tmp/summary.csv'

download_op = GoogleCloudStorageDownloadOperator(
    task_id="{}_download".format(TASK_BASENAME),
    bucket=constants.GCS_BUCKET_NAME,
    object="{}/{}".format(TASK_BASENAME, GCS_FILENAME),
    filename=LOCAL_FILENAME,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    dag=dag
)

download_op.set_upstream(elasticsearch_to_gcs_op)

SUMMARY = """Hard Hat Hub Summary {{ ds }}"""

HTML_CONTENT = """
<h2>Execution Date: {{ ds }}</h2>
Attached is the summary csv.
"""

email_op = EmailOperator(
    task_id="{}_email".format(TASK_BASENAME),
    to=constants.SUMMARY_EMAILS,
    # cc=[],
    # bcc=[],
    subject=SUMMARY,
    html_content=HTML_CONTENT,
    files=[LOCAL_FILENAME],
    dag=dag
)

email_op.set_upstream(download_op)


def cleanup():
    logging.info("Deleting {}".format(LOCAL_FILENAME))
    os.remove(LOCAL_FILENAME)


cleanup_op = PythonOperator(
    task_id="{}_cleanup".format(TASK_BASENAME),
    python_callable=cleanup,
    dag=dag
)

cleanup_op.set_upstream(email_op)

#################################################
