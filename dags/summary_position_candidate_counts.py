import json
import requests
import logging
import time

from airflow import DAG
from airflow.operators import PythonOperator
from airflow.hooks import PostgresHook
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile
from datetime import datetime

# custom
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'summary_position_candidate_counts'
GCS_FILENAME = 'summary_position_candidate_counts.json'

POSTGRES_QUERY = """
    SELECT
      employer_id,
      id as position_id,
      positions.name as position_name
    FROM
      positions
    WHERE
      terminated_at IS NULL
"""

BQ_SCHEMA = [
    {"name": "employer_id", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "position_id", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "position_name", "type": "STRING", "mode": "NULLABLE"},
    {"name": "locked_total_count", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "locked_passive_count", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "unlocked_total_count", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "unlocked_passive_count", "type": "INTEGER", "mode": "NULLABLE"},
    {"name": "date", "type": "TIMESTAMP", "mode": "NULLABLE"}
]

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

dag = DAG(TASK_BASENAME, schedule_interval='0 7 * * *', default_args=default_args)


def elasticsearch_to_gcs():
    positions = _query_postgres()
    rows = _query_elasticsearch(positions)
    files_to_upload = _write_local_data_files(rows)

    for file_handle in files_to_upload.values():
        file_handle.flush()

    _upload_to_gcs(files_to_upload)

    for file_handle in files_to_upload.values():
        file_handle.close()


def _query_postgres():
    postgres_hook = PostgresHook(postgres_conn_id=constants.POSTGRES_CONN_ID)
    conn = postgres_hook.get_conn()
    cursor = conn.cursor()
    cursor.execute(POSTGRES_QUERY)
    rows = cursor.fetchall()
    schema = map(lambda schema_tuple: schema_tuple[0], cursor.description)

    postgres_row_dicts = []
    for row in rows:
        row_dict = dict(zip(schema, row))
        postgres_row_dicts.append(row_dict)

    return postgres_row_dicts


def _query_elasticsearch_api(employer_id, position_id, unlocked=False):
    url = '{}/employers/{}/positions/{}/position_search_results'.format(
        constants.H3_POSITIONS_API_URL_BASE,
        employer_id,
        position_id
    )

    url_params = {
        'user_id': 1,
        'api_auth_key': constants.H3_API_AUTH_KEY
    }

    if unlocked:
        url_params['optional_search_criteria'] = json.dumps({'dropdownCriteria': 'connected'})

    return requests.get(url, params=url_params)


def _query_elasticsearch(positions):
    rows = []
    now = time.mktime(datetime.today().timetuple())

    for item in positions:
        employer_id = item['employer_id']
        position_id = item['position_id']

        response = _query_elasticsearch_api(employer_id, position_id)

        if response.status_code != 200:
            logging.warn("Error response (#{}) for unlocked search count for position (#{})"
                         .format(response.status_code, position_id))
            continue

        locked_position_results_json = response.json()

        if not ('result_counts' in locked_position_results_json and
                        'activeCount' in locked_position_results_json['result_counts'] and
                        'passiveCount' in locked_position_results_json['result_counts']):
            logging.warn("Missing locked search counts for position (#{})".format(position_id))
            continue

        response = _query_elasticsearch_api(employer_id, position_id, unlocked=True)

        # TODO: think about leaving values null rather than losing data by skipping
        if response.status_code != 200:
            logging.warn("Error response (#{}) for unlocked search count for position (#{})"
                         .format(response.status_code, position_id))
            continue

        unlocked_position_results_json = response.json()

        if not ('result_counts' in unlocked_position_results_json and
                        'activeCount' in unlocked_position_results_json['result_counts'] and
                        'passiveCount' in unlocked_position_results_json['result_counts']):
            logging.warn("Missing unlocked search counts for position (#{})".format(position_id))
            continue

        locked_total_count = (locked_position_results_json['result_counts']['activeCount'] +
                              locked_position_results_json['result_counts']['passiveCount'])

        locked_passive_count = locked_position_results_json['result_counts']['passiveCount']

        unlocked_total_count = (unlocked_position_results_json['result_counts']['activeCount'] +
                                unlocked_position_results_json['result_counts']['passiveCount'])

        unlocked_passive_count = unlocked_position_results_json['result_counts']['passiveCount']

        count_dict = {
            'employer_id': employer_id,
            'position_id': position_id,
            'position_name': item['position_name'],
            'locked_total_count': locked_total_count,
            'locked_passive_count': locked_passive_count,
            'unlocked_total_count': unlocked_total_count,
            'unlocked_passive_count': unlocked_passive_count,
            'date': now
        }

        rows.append(count_dict)

    return rows


def _write_local_data_files(rows):
    filename = "{}/{}".format(TASK_BASENAME, GCS_FILENAME)
    approx_max_file_size_bytes = 1073741824

    file_no = 0
    tmp_file_handle = NamedTemporaryFile(delete=True)
    tmp_file_handles = {filename.format(file_no): tmp_file_handle}

    for row_dict in rows:
        # TODO validate that row isn't > 2MB. BQ enforces a hard row size of 2MB.
        json.dump(row_dict, tmp_file_handle)

        # Append newline to make dumps BigQuery compatible.
        tmp_file_handle.write('\n')

        # Stop if the file exceeds the file size limit.
        if tmp_file_handle.tell() >= approx_max_file_size_bytes:
            file_no += 1
            tmp_file_handle = NamedTemporaryFile(delete=True)
            tmp_file_handles[filename.format(file_no)] = tmp_file_handle

    return tmp_file_handles


def _upload_to_gcs(files_to_upload):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=constants.GCP_CONN_ID,
                                  delegate_to=None)
    for object, tmp_file_handle in files_to_upload.items():
        hook.upload(constants.GCS_BUCKET_NAME, object, tmp_file_handle.name, 'application/json')


gcs_to_bq_op = GoogleCloudStorageToBigQueryOperator(
    bucket=constants.GCS_BUCKET_NAME,
    source_objects=["{}/{}".format(TASK_BASENAME, GCS_FILENAME)],
    destination_project_dataset_table="{}.summary_position_candidate_counts".format(constants.BQ_DATASET_NAME),
    schema_fields=BQ_SCHEMA,
    source_format='NEWLINE_DELIMITED_JSON',
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    task_id="{}_bq".format(TASK_BASENAME),
    dag=dag
)

elasticsearch_to_gcs_op = PythonOperator(
    task_id="{}_python".format(TASK_BASENAME),
    python_callable=elasticsearch_to_gcs,
    dag=dag
)

gcs_to_bq_op.set_upstream(elasticsearch_to_gcs_op)
