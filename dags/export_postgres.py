from airflow import DAG
from airflow.operators import DummyOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from datetime import datetime, timedelta
import logging
import glob
import os
import json

# custom
from postgres_to_gcs_operator import PostgresToGoogleCloudStorageOperator
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'export_postgres'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

dag = DAG(TASK_BASENAME, schedule_interval='0 5 * * *', default_args=default_args)

first = DummyOperator(
    task_id="{}_first".format(TASK_BASENAME),
    dag=dag
)

middle = DummyOperator(
    task_id="{}_middle".format(TASK_BASENAME),
    dag=dag
)

last = DummyOperator(
    task_id="{}_last".format(TASK_BASENAME),
    dag=dag
)

sql_files = glob.glob("{}/*.sql".format(constants.POSTGRES_QUERIES_DIR))

for sql_file in sql_files:
    filename = os.path.splitext(os.path.basename(sql_file))[0]

    csv_file = "{}_data.json".format(filename)

    with open(sql_file, 'r') as f:
        sql_data = f.read()
        sql_commands = sql_data.split(';')

    psql_to_gcs_op = PostgresToGoogleCloudStorageOperator(
        sql=sql_commands[0],
        bucket=constants.GCS_BUCKET_NAME,
        filename="{}/{}".format(TASK_BASENAME, csv_file),
        approx_max_file_size_bytes=1073741824,
        postgres_conn_id=constants.POSTGRES_CONN_ID,
        google_cloud_storage_conn_id=constants.GCP_CONN_ID,
        task_id="{}_{}_export".format(TASK_BASENAME, filename),
        retries=3,
        dag=dag
    )

    psql_to_gcs_op.set_upstream(first)

    middle.set_upstream(psql_to_gcs_op)

    schema_file = "{}/{}.json".format(constants.POSTGRES_SCHEMAS_DIR, filename)

    with open(schema_file, 'r') as f:
        schema_data = f.read()

    schema = json.loads(schema_data)

    gcs_to_bq_op = GoogleCloudStorageToBigQueryOperator(
        bucket=constants.GCS_BUCKET_NAME,
        source_objects=["{}/{}".format(TASK_BASENAME, csv_file)],
        destination_project_dataset_table="{}.{}".format(constants.BQ_DATASET_NAME, filename),
        schema_fields=schema,
        source_format='NEWLINE_DELIMITED_JSON',
        write_disposition='WRITE_TRUNCATE',
        bigquery_conn_id=constants.GCP_CONN_ID,
        google_cloud_storage_conn_id=constants.GCP_CONN_ID,
        task_id="{}_{}_import".format(TASK_BASENAME, filename),
        dag=dag
    )

    gcs_to_bq_op.set_upstream(middle)

    last.set_upstream(gcs_to_bq_op)
