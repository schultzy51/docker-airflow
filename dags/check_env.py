import logging

from airflow import DAG
from airflow.operators import PythonOperator
from datetime import datetime, timedelta
from airflow import configuration

# custom
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'check_env'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

# Don't schedule
dag = DAG(TASK_BASENAME, schedule_interval=None, default_args=default_args)

CUSTOM_ENV_VARS = [
    'gcp_conn_id',
    'gcs_bucket_name',
    'gcs_empty_json',
    'bq_dataset_name',
    's3_conn_id',
    's3_bucket_name',
    'postgres_conn_id',
    'postgres_queries_dir',
    'postgres_schemas_dir',
    'dev_email',
    'h3_positions_api_url_base',
    'h3_api_auth_key'
]

CUSTOM_CONSTANTS = [
    constants.GCP_CONN_ID,
    constants.GCS_BUCKET_NAME,
    constants.GCS_EMPTY_JSON,
    constants.BQ_DATASET_NAME,
    constants.S3_CONN_ID,
    constants.S3_BUCKET_NAME,
    constants.POSTGRES_CONN_ID,
    constants.POSTGRES_QUERIES_DIR,
    constants.POSTGRES_SCHEMAS_DIR,
    constants.DEV_EMAIL,
    constants.REPORT_EMAIL,
    constants.DEFAULT_EMAILS,
    constants.SUMMARY_EMAILS,
    constants.H3_POSITIONS_API_URL_BASE,
    constants.H3_API_AUTH_KEY
]


def check_env():
    logging.info('##################################')
    logging.info('# Checking environment variables #')
    logging.info('##################################')
    for var in CUSTOM_ENV_VARS:
        logging.info("{}: {}".format(var, configuration.get('custom', var)))

    logging.info('##################################')
    logging.info('#      Checking constants        #')
    logging.info('##################################')
    for constant in CUSTOM_CONSTANTS:
        logging.info(constant)


cleanup_op = PythonOperator(
    task_id="{}_log".format(TASK_BASENAME),
    python_callable=check_env,
    dag=dag
)
