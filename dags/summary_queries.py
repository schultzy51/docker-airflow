import logging

from airflow import DAG
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from datetime import datetime, timedelta

# custom
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'summary_queries'

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

dag = DAG(TASK_BASENAME, schedule_interval='0 7 * * *', default_args=default_args)

BQ_POSITIONS_TABLE_NAME = "{}.{}".format(constants.BQ_DATASET_NAME, 'summary_positions_count')
BQ_POSITIONS_QUERY = """
    SELECT
      COUNT(*) AS positions_total,
      SUM(IF(terminated_at IS NULL, 1, 0)) AS positions_active,
      CURRENT_TIMESTAMP() AS date
    FROM {}.positions
""".format(constants.BQ_DATASET_NAME)

positions_count = BigQueryOperator(
    bql=BQ_POSITIONS_QUERY,
    destination_dataset_table=BQ_POSITIONS_TABLE_NAME,
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    task_id="{}_positions_count".format(TASK_BASENAME),
    dag=dag
)

#################################################

BQ_EMPLOYER_SIGNUPS_TABLE_NAME = "{}.{}".format(constants.BQ_DATASET_NAME, 'summary_employer_signups_count')
BQ_EMPLOYER_SIGNUPS_QUERY = """
    SELECT
      COUNT(*) AS signups,
      SUM(IF(cp.company_name IS NOT NULL, 1, 0)) AS has_profile,
      SUM(IF(eu.dismissed_tutorial IS NOT NULL, 1, 0)) AS dismissed_tutorial,
      CURRENT_TIMESTAMP() AS date
    FROM {0}.employers AS e
      LEFT JOIN {0}.company_profiles AS cp ON cp.employer_id = e.id
      INNER JOIN {0}.employer_users AS eu ON eu.employer_id = e.id
      INNER JOIN {0}.users u ON u.id = eu.user_id
    WHERE e.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -1, 'WEEK')
      AND eu.role = 'Admin'
      AND u.first_name <> u.last_name -- Filter Russian bots (first_name == last_name)
""".format(constants.BQ_DATASET_NAME)

employer_signup_count = BigQueryOperator(
    bql=BQ_EMPLOYER_SIGNUPS_QUERY,
    destination_dataset_table=BQ_EMPLOYER_SIGNUPS_TABLE_NAME,
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    task_id="{}_employer_signups_count".format(TASK_BASENAME),
    dag=dag
)

#################################################

BQ_JOBSEEKER_SIGNUPS_TABLE_NAME = "{}.{}".format(constants.BQ_DATASET_NAME, 'summary_jobseeker_signups_count')
BQ_JOBSEEKER_SIGNUPS_QUERY = """
    SELECT
      COUNT(*) AS wizards_started,
      SUM(IF(p.wizard_completed_at IS NOT NULL, 1, 0)) AS wizard_completed,
      SUM(IF(p.resume IS NOT NULL, 1, 0)) AS has_resume,
      CURRENT_TIMESTAMP() AS date
    FROM {0}.users u
    INNER JOIN {0}.professional_profiles p ON p.user_id=u.id
    WHERE p.created_at > DATE_ADD(CURRENT_TIMESTAMP(), -1, 'WEEK');
""".format(constants.BQ_DATASET_NAME)

employer_signup_count = BigQueryOperator(
    bql=BQ_JOBSEEKER_SIGNUPS_QUERY,
    destination_dataset_table=BQ_JOBSEEKER_SIGNUPS_TABLE_NAME,
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    task_id="{}_jobseeker_signups_count".format(TASK_BASENAME),
    dag=dag
)
