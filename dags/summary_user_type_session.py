import logging

from airflow import DAG
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from datetime import datetime, timedelta

# custom
from ga_to_gcs_operator import GoogleAnalyticsToGoogleCloudStorageOperator
import constants

LOG = logging.getLogger(__name__)

days_ago = datetime.combine(datetime.today() - timedelta(1), datetime.min.time())

TASK_BASENAME = 'summary_user_type_session'
VIEW_ID = '95035718'
AVERAGE_SESSION_DURATION_FOR_USER_TYPE_QUERY = {
    'reportRequests': [
        {
            'viewId': VIEW_ID,
            'dateRanges': [
                {'startDate': '7daysAgo', 'endDate': 'today'},
                {'startDate': '30daysAgo', 'endDate': 'today'}
            ],
            'metrics': [
                {'expression': 'ga:avgSessionDuration'}
            ],
            'dimensions': [
                {'name': 'ga:dimension3'}
            ]
        }
    ]
}

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago,
    'email': constants.DEFAULT_EMAILS,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(seconds=15),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 7, 1),
}

dag = DAG(TASK_BASENAME, schedule_interval='0 7 * * *', default_args=default_args)

GCS_FILENAME = "{}/{}".format(TASK_BASENAME, "summary_user_type_session.json")

ga_to_gcs_op = GoogleAnalyticsToGoogleCloudStorageOperator(
    request=AVERAGE_SESSION_DURATION_FOR_USER_TYPE_QUERY,
    bucket=constants.GCS_BUCKET_NAME,
    filename=GCS_FILENAME,
    approx_max_file_size_bytes=1073741824,
    google_analytics_conn_id=constants.GCP_CONN_ID,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    task_id="{}_report".format(TASK_BASENAME),
    retries=3,
    dag=dag
)

# create big query table to hold my values 
BQ_SCHEMA = [
    {"name": "ranges",
     "type": "RECORD",
     "mode": "REPEATED",
     "fields":
         [
             {"name": "range", "type": "INTEGER", "mode": "NULLABLE"},
             {"name": "ga_avgSessionDuration", "type": "FLOAT", "mode": "NULLABLE"}
         ]
     },
    {"name": "date", "type": "TIMESTAMP", "mode": "NULLABLE"},
    {"name": "ga_dimension3", "type": "STRING", "mode": "NULLABLE"}
]

BQ_TABLE_NAME = "{}.{}".format(constants.BQ_DATASET_NAME, 'summary_user_type_session')

# this makes the table
gcs_to_bq_op = GoogleCloudStorageToBigQueryOperator(
    bucket=constants.GCS_BUCKET_NAME,
    source_objects=[GCS_FILENAME],
    destination_project_dataset_table=BQ_TABLE_NAME,
    schema_fields=BQ_SCHEMA,
    source_format='NEWLINE_DELIMITED_JSON',
    write_disposition='WRITE_APPEND',
    bigquery_conn_id=constants.GCP_CONN_ID,
    google_cloud_storage_conn_id=constants.GCP_CONN_ID,
    task_id="{}_import".format(TASK_BASENAME),
    dag=dag
)

gcs_to_bq_op.set_upstream(ga_to_gcs_op)
