from airflow import configuration

# google cloud platform
GCP_CONN_ID = configuration.get('custom', 'gcp_conn_id')
GCS_BUCKET_NAME = configuration.get('custom', 'gcs_bucket_name')
GCS_EMPTY_JSON = configuration.get('custom', 'gcs_empty_json')
BQ_DATASET_NAME = configuration.get('custom', 'bq_dataset_name')

# aws
S3_CONN_ID = configuration.get('custom', 's3_conn_id')
S3_BUCKET_NAME = configuration.get('custom', 's3_bucket_name')

# postgres
POSTGRES_CONN_ID = configuration.get('custom', 'postgres_conn_id')
POSTGRES_QUERIES_DIR = configuration.get('custom', 'postgres_queries_dir')
POSTGRES_SCHEMAS_DIR = configuration.get('custom', 'postgres_schemas_dir')

# other
DEV_EMAIL = configuration.get('custom', 'dev_email')
REPORT_EMAIL = configuration.get('custom', 'report_email')
DEFAULT_EMAILS = [DEV_EMAIL]
SUMMARY_EMAILS = [REPORT_EMAIL, DEV_EMAIL]
H3_POSITIONS_API_URL_BASE = configuration.get('custom', 'h3_positions_api_url_base')
H3_API_AUTH_KEY = configuration.get('custom', 'h3_api_auth_key')
