# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging

from airflow.contrib.hooks.gcp_api_base_hook import GoogleCloudBaseHook
from apiclient.discovery import build

logging.getLogger("google_cloud_storage").setLevel(logging.INFO)


class GoogleAnalyticsHook(GoogleCloudBaseHook):
    """
    Interact with Google Analytics. This hook uses the Google Cloud Platform
    connection.
    """

    def __init__(self,
                 google_analytics_conn_id='google_analytics_default',
                 delegate_to=None):
        super(GoogleAnalyticsHook, self).__init__(google_analytics_conn_id, delegate_to)

    def get_conn(self):
        """
        Returns a Google Cloud Storage service object.
        """
        http_authorized = self._authorize()
        return build('analytics', 'v4', http=http_authorized)

    def get_reports(self, request):
        """
        Get a report from Google Analytics.

        :param request: The query to run
        :type request: string
        """
        service = self.get_conn()
        return service.reports().batchGet(body=request).execute()
