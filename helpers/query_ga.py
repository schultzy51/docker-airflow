# Helper script for testing the google analytics reporting API v4 queries

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

import httplib2

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')
KEY_FILE_LOCATION = 'secret/airflow_google_h3_data.json'
VIEW_ID = '95035718'
TEST_QUERY = {
    'reportRequests': [
        {
            'viewId': VIEW_ID,
            'dateRanges': [{'startDate': '7daysAgo', 'endDate': 'today'}],
            'metrics': [{'expression': 'ga:sessions'}]
        }]
}

QUERY = {
    'reportRequests': [
        {
            'viewId': VIEW_ID,
            'dateRanges': [
                {'startDate': '61daysAgo', 'endDate': 'today'}
            ],
            'metrics': [
                {'expression': 'ga:pageviews'}
            ],
            'dimensions': [
                # {'name': 'ga:pagePathLevel2'},
                {'name': 'ga:pagePathLevel3'}
            ],
            'orderBys': [
                {
                    "fieldName": "ga:pageviews",
                    "sortOrder": "DESCENDING"
                }
            ],
            "metricFilterClauses": [{
                "filters": [{
                    "metricName": "ga:pageviews",
                    "operator": "GREATER_THAN",
                    "comparisonValue": "0"
                }]
            }],
            "dimensionFilterClauses": [
                {
                    "filters": [
                        {
                            "dimensionName": "ga:pagePathLevel2",
                            "operator": "EXACT",
                            "expressions": ["www.hardhathub.com"]
                        },
                        {
                            "dimensionName": "ga:pagePathLevel2",
                            "operator": "EXACT",
                            "expressions": ["/company_profiles/"]
                        }
                    ]
                }
            ]
        }
    ]
}


def initialize_analyticsreporting():
    """Initializes an analyticsreporting service object.

    Returns:
      analytics an authorized analyticsreporting service object.
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        KEY_FILE_LOCATION, scopes=SCOPES)

    http = credentials.authorize(httplib2.Http())

    # Build the service object.
    analytics = build('analytics', 'v4', http=http, discoveryServiceUrl=DISCOVERY_URI)

    return analytics


def get_report(analytics):
    # Use the Analytics Service Object to query the Analytics Reporting API V4.
    return analytics.reports().batchGet(
        body=QUERY
    ).execute()


def print_response(response):
    """Parses and prints the Analytics Reporting API V4 response"""

    for report in response.get('reports', []):
        columnHeader = report.get('columnHeader', {})
        dimensionHeaders = columnHeader.get('dimensions', [])
        metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])
        rows = report.get('data', {}).get('rows', [])

        for row in rows:
            dimensions = row.get('dimensions', [])
            dateRangeValues = row.get('metrics', [])

            for header, dimension in zip(dimensionHeaders, dimensions):
                print header + ': ' + dimension

            for i, values in enumerate(dateRangeValues):
                print 'Date range (' + str(i) + ')'
                for metricHeader, value in zip(metricHeaders, values.get('values')):
                    print metricHeader.get('name') + ': ' + value


def main():
    analytics = initialize_analyticsreporting()
    response = get_report(analytics)
    print_response(response)


if __name__ == '__main__':
    main()
