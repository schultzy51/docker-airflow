# Helper script for generating the bigquery schemas

import psycopg2
import glob
import os
import json
import sys

def type_map(postgres_type):
  d = {
    16: 'BOOLEAN',  # BOOLOID
    # 17: '',  # BYTEAOID
    # 18: '',  # CHAROID
    # 19: '',  # NAMEOID
    20: 'INTEGER',  # INT8OID
    21: 'INTEGER',  # INT2OID
    # 22: '',  # INT2VECTOROID
    23: 'INTEGER',  # INT4OID
    # 24: '',  # REGPROCOID
    # 25: '',  # TEXTOID
    # 26: '',  # OIDOID
    # 27: '',  # TIDOID
    # 28: '',  # XIDOID
    # 29: '',  # CIDOID
    # 30: '',  # OIDVECTOROID
    # 71: '',  # PG_TYPE_RELTYPE_OID
    # 75: '',  # PG_ATTRIBUTE_RELTYPE_OID
    # 81: '',  # PG_PROC_RELTYPE_OID
    # 83: '',  # PG_CLASS_RELTYPE_OID
    # 600: '',  # POINTOID
    # 601: '',  # LSEGOID
    # 602: '',  # PATHOID
    # 603: '',  # BOXOID
    # 604: '',  # POLYGONOID
    # 628: '',  # LINEOID
    700: 'FLOAT',  # FLOAT4OID
    701: 'FLOAT',  # FLOAT8OID
    702: 'TIMESTAMP',  # ABSTIMEOID
    703: 'TIMESTAMP',  # RELTIMEOID
    # 704: '',  # TINTERVALOID
    # 705: '',  # UNKNOWNOID
    # 718: '',  # CIRCLEOID
    # 790: '',  # CASHOID
    # 829: '',  # MACADDROID
    # 869: '',  # INETOID
    # 650: '',  # CIDROID
    # 1007: '',  # INT4ARRAYOID
    # 1033: '',  # ACLITEMOID
    # 1042: '',  # BPCHAROID
    # 1043: '',  # VARCHAROID
    1082: 'TIMESTAMP',  # DATEOID
    1083: 'TIMESTAMP',  # TIMEOID
    1114: 'TIMESTAMP',  # TIMESTAMPOID
    1184: 'TIMESTAMP',  # TIMESTAMPTZOID
    # 1186: '',  # INTERVALOID
    # 1266: '',  # TIMETZOID
    1560: 'INTEGER',  # BITOID
    1562: 'INTEGER',  # VARBITOID
    1700: 'FLOAT',  # NUMERICOID
    # 1790: '',  # REFCURSOROID
    # 2202: '',  # REGPROCEDUREOID
    # 2203: '',  # REGOPEROID
    # 2204: '',  # REGOPERATOROID
    # 2205: '',  # REGCLASSOID
    # 2206: '',  # REGTYPEOID
    # 2249: '',  # RECORDOID
    # 2275: '',  # CSTRINGOID
    # 2276: '',  # ANYOID
    # 2277: '',  # ANYARRAYOID
    # 2278: '',  # VOIDOID
    # 2279: '',  # TRIGGEROID
    # 2280: '',  # LANGUAGE_HANDLEROID
    # 2281: '',  # INTERNALOID
    # 2282: '',  # OPAQUEOID
    # 2283: ''  # ANYELEMENTOID
  }

  return d[postgres_type] if postgres_type in d else 'STRING'

sql_files = glob.glob("dags/queries/*.sql")

for sql_file in sql_files:
  filename = os.path.splitext(os.path.basename(sql_file))[0]
  nullable = {}

  with open(sql_file, 'r') as f:
    raw_data = f.read()
    sql_commands = raw_data.split(';')

  try:
    conn = psycopg2.connect("dbname='dug0sdrvvebo2' user='ucfqvbvvdkhre0' host='ec2-52-206-176-15.compute-1.amazonaws.com' password='paora5b8m0lolae8rs3t6pc5en'")
    cur = conn.cursor()

    cur.execute("""SELECT column_name, is_nullable from information_schema.columns where table_name='{}'""".format(filename))

    rows = cur.fetchall()

    for row in rows:
      column_name, is_nullable = row
      nullable[column_name] = (is_nullable == 'YES')

    cur.execute(sql_commands[0])
    schema = []
    for field in cur.description:
      # See PEP 249 for details about the description tuple.
      field_name = field[0]
      field_type = type_map(field[1])
      # Always allow TIMESTAMP to be nullable. MySQLdb returns None types
      # for required fields because some MySQL timestamps can't be
      # represented by Python's datetime (e.g. 0000-00-00 00:00:00).
      # TODO: verify if this is necessary for postgres
      field_mode = 'NULLABLE' if field[6] or nullable[field_name] else 'REQUIRED'
      schema.append({
        'name': field_name,
        'type': field_type,
        'mode': field_mode,
      })

    with open("dags/schemas/{}.json".format(filename), 'w') as f:
      f.write(json.dumps(schema, indent=2, separators=(',', ': ')))
      f.write("\n")

  except Exception as e:
    print e