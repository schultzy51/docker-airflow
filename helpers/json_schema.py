# Helper script for parsing downloaded bigquery schemas

data = []
with open('users_schema.json', 'r') as f:
    data = f.read()

text = ''

schema = eval(data)
for field in schema:
    text += "{}:{},".format(field['name'], field['type'])

print text
