# Helper script for testing the elasticsearch API

import json
import requests

H3_POSITIONS_API_URL_BASE = 'https://hardhathub-uat.herokuapp.com'
H3_API_AUTH_KEY = 'bed1a215-be34-426f-bfe3-d8c599a90b6a'


def _query_elasticsearch_api(employer_id, position_id, unlocked=False):
    url = '{}/employers/{}/positions/{}/position_search_results'.format(
        H3_POSITIONS_API_URL_BASE,
        employer_id,
        position_id
    )

    url_params = {
        'user_id': 1,
        'api_auth_key': H3_API_AUTH_KEY
    }

    if unlocked:
        url_params['optional_search_criteria'] = json.dumps({'dropdownCriteria': 'connected'})

    return requests.get(url, params=url_params)


employer_id = 38
position_id = 36

print 'LOCKED'
response = _query_elasticsearch_api(employer_id, position_id, unlocked=False)
if response.status_code == 200:
    print response.json()['result_counts']

print 'UNLOCKED'
response = _query_elasticsearch_api(employer_id, position_id, unlocked=True)
if response.status_code == 200:
    print response.json()['result_counts']
