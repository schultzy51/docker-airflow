# Helper script for generating the postgres queries

import psycopg2

data = {}

try:
    conn = psycopg2.connect("dbname='dug0sdrvvebo2' user='ucfqvbvvdkhre0' host='ec2-52-206-176-15.compute-1.amazonaws.com' password='paora5b8m0lolae8rs3t6pc5en'")
    cur = conn.cursor()

    cur.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema='public' order by table_name""")

    tables = [row[0] for row in cur.fetchall()]

    for table in tables:
        cur.execute("""SELECT column_name from information_schema.columns where table_name='{}'""".format(table))

        data[table] = [row[0] for row in cur.fetchall()]
except Exception as e:
    print e

tables = data.keys()
tables.sort()
tables.remove('pg_stat_statements')

for table in tables:
    with open("dags/queries/{}.sql".format(table), 'w') as f:
        f.write("SELECT\n")

        for column in data[table][:-1]:
            f.write("  \"{}\",\n".format(column))

        f.write("  \"{}\"\n".format(data[table][-1]))
        f.write("FROM\n")
        f.write("  \"{}\";\n".format(table))
