SELECT
  uid,
  email,
  sign_in_count,
  current_sign_in_at,
  last_sign_in_at,
  current_sign_in_ip,
  last_sign_in_ip,
  confirmed_at,
  confirmation_sent_at,
  unconfirmed_email,
  created_at,
  updated_at,
  first_name,
  last_name,
  phone_number,
  provider,
  invitation_created_at,
  invitation_sent_at,
  invitation_accepted_at,
  invitation_limit,
  invited_by_id,
  invited_by_type,
  welcomed,
  will_receive_account_emails,
  terminated_at,
  terminated_by,
  last_activity_at,
  json_build_object(
      'id', profile_id,
      'user_id', user_id,
      'created_at', profile_created_at,
      'status', status,
      'category', json_build_object(
          'id', category_id,
          'key', ckey),
      'category_functions', array_to_json(cfs),
      'popularity_metric', json_build_object(
          'employer_count', employer_count,
          'position_count', position_count
      ),
      'metro_areas', array_to_json(metros)
  ) AS professional_profile


FROM
  (SELECT
     users.id                          AS uid,
     users.email,
     users.sign_in_count,
     users.current_sign_in_at,
     users.last_sign_in_at,
     users.current_sign_in_ip,
     users.last_sign_in_ip,
     users.confirmed_at,
     users.confirmation_sent_at,
     users.unconfirmed_email,
     users.created_at,
     users.updated_at,
     users.first_name,
     users.last_name,
     users.phone_number,
     users.provider,
     users.invitation_created_at,
     users.invitation_sent_at,
     invitation_accepted_at,
     users.invitation_limit,
     users.invited_by_id,
     users.invited_by_type,
     users.welcomed,
     users.will_receive_account_emails,
     users.terminated_at,
     users.terminated_by,
     users.last_activity_at,

     professional_profiles.id          AS profile_id,
     professional_profiles.user_id,
     professional_profiles.status,
     professional_profiles.created_at  AS profile_created_at,

     categories.id                     AS category_id,
     categories.key                    AS ckey,

     popularity_metrics.employer_count,
     popularity_metrics.position_count,
     array_agg(category_functions) FILTER (WHERE category_functions IS NOT NULL) AS cfs,
     array_agg(metro_areas)        FILTER (WHERE metro_areas IS NOT NULL) AS metros
   FROM users
     INNER JOIN professional_profiles ON users.id = professional_profiles.user_id
     LEFT OUTER JOIN categories ON professional_profiles.category_id = categories.id
     LEFT OUTER JOIN category_functions_professional_profiles AS cfpp
       ON cfpp.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN category_functions ON category_functions.id = cfpp.category_function_id
     LEFT OUTER JOIN popularity_metrics ON popularity_metrics.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN metro_areas_professional_profiles AS mapp
       ON mapp.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN metro_areas ON mapp.metro_area_id = professional_profiles.id
   GROUP BY users.id,
            users.email,
            users.sign_in_count,
            users.current_sign_in_at,
            users.last_sign_in_at,
            users.current_sign_in_ip,
            users.last_sign_in_ip,
            users.confirmed_at,
            users.confirmation_sent_at,
            users.unconfirmed_email,
            users.created_at,
            users.updated_at,
            users.first_name,
            users.last_name,
            users.phone_number,
            users.provider,
            users.invitation_created_at,
            users.invitation_sent_at,
            users.invitation_limit,
            users.invited_by_id,
            users.invited_by_type,
            users.welcomed,
            users.will_receive_account_emails,
            users.terminated_at,
            users.terminated_by,
            users.last_activity_at,
            professional_profiles.id ,
            professional_profiles.user_id,
            professional_profiles.status,
            professional_profiles.created_at,
            categories.id,
            categories.key,
            popularity_metrics.employer_count,
            popularity_metrics.position_count
   LIMIT 1
  ) AS t;

-- Favor this for first pass. It's easier --
SELECT
  uid,
  email,
  sign_in_count,
  current_sign_in_at,
  last_sign_in_at,
  current_sign_in_ip,
  last_sign_in_ip,
  confirmed_at,
  confirmation_sent_at,
  unconfirmed_email,
  created_at,
  updated_at,
  first_name,
  last_name,
  phone_number,
  provider,
  invitation_created_at,
  invitation_sent_at,
  invitation_accepted_at,
  invitation_limit,
  invited_by_id,
  invited_by_type,
  welcomed,
  will_receive_account_emails,
  terminated_at,
  terminated_by,
  last_activity_at,
  array_to_json(profile) AS profile,
  json_build_object(
    'id', category_id,
    'key', ckey) AS category,
  array_to_json(cfs) AS category_functions,
  json_build_object(
    'employer_count', employer_count,
    'position_count', position_count) AS popularity_metric,
  array_to_json(metros) AS metro_areas


FROM
  (SELECT
     users.id                          AS uid,
     users.email,
     users.sign_in_count,
     users.current_sign_in_at,
     users.last_sign_in_at,
     users.current_sign_in_ip,
     users.last_sign_in_ip,
     users.confirmed_at,
     users.confirmation_sent_at,
     users.unconfirmed_email,
     users.created_at,
     users.updated_at,
     users.first_name,
     users.last_name,
     users.phone_number,
     users.provider,
     users.invitation_created_at,
     users.invitation_sent_at,
     invitation_accepted_at,
     users.invitation_limit,
     users.invited_by_id,
     users.invited_by_type,
     users.welcomed,
     users.will_receive_account_emails,
     users.terminated_at,
     users.terminated_by,
     users.last_activity_at,
     categories.id                     AS category_id,
     categories.key                    AS ckey,
     popularity_metrics.employer_count,
     popularity_metrics.position_count,
     array_agg(professional_profiles)  AS profile,
     array_agg(category_functions) FILTER (WHERE category_functions IS NOT NULL) AS cfs,
     array_agg(metro_areas)        FILTER (WHERE metro_areas IS NOT NULL) AS metros
   FROM users
     INNER JOIN professional_profiles ON users.id = professional_profiles.user_id
     LEFT OUTER JOIN categories ON professional_profiles.category_id = categories.id
     LEFT OUTER JOIN category_functions_professional_profiles AS cfpp ON cfpp.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN category_functions ON category_functions.id = cfpp.category_function_id
     LEFT OUTER JOIN popularity_metrics ON popularity_metrics.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN metro_areas_professional_profiles AS mapp
       ON mapp.professional_profile_id = professional_profiles.id
     LEFT OUTER JOIN metro_areas ON mapp.metro_area_id = professional_profiles.id
   GROUP BY users.id,
            users.email,
            users.sign_in_count,
            users.current_sign_in_at,
            users.last_sign_in_at,
            users.current_sign_in_ip,
            users.last_sign_in_ip,
            users.confirmed_at,
            users.confirmation_sent_at,
            users.unconfirmed_email,
            users.created_at,
            users.updated_at,
            users.first_name,
            users.last_name,
            users.phone_number,
            users.provider,
            users.invitation_created_at,
            users.invitation_sent_at,
            users.invitation_limit,
            users.invited_by_id,
            users.invited_by_type,
            users.welcomed,
            users.will_receive_account_emails,
            users.terminated_at,
            users.terminated_by,
            users.last_activity_at,
            categories.id,
            categories.key,
            popularity_metrics.employer_count,
            popularity_metrics.position_count
   LIMIT 1
  ) AS t;
