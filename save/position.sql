SELECT
  pid,
  eid,
  name,
  search_criteria,
  terminated_at,
  created_at,
  updated_at,
  description,
  advanced_search_status,
  json_build_object(
    'id', employer_id,
    'employer_name', employer_name,
    'created_at', employer_created_at,
    'updated_at', employer_updated_at,
    'wizard_completed_at', wizard_completed_at,
    'hidden_at', hidden_at,
    'brand_score', brand_score
  ) AS employer
FROM
  (SELECT
    positions.id AS pid,
    positions.employer_id AS eid,
    positions.name,
    positions.search_criteria,
    positions.terminated_at,
    positions.created_at,
    positions.updated_at,
    positions.description,
    positions.advanced_search_status,
    employers.id AS employer_id,
    employers.name AS employer_name,
    employers.created_at AS employer_created_at,
    employers.updated_at AS employer_updated_at,
    employers.wizard_completed_at,
    employers.hidden_at,
    employers.brand_score
  FROM positions
    INNER JOIN employers on positions.employer_id = employers.id
    LEFT OUTER JOIN matches ON positions.employer_id = matches.employer_id
  GROUP BY  positions.id,
            positions.employer_id,
            positions.name,
            positions.search_criteria,
            positions.terminated_at,
            positions.created_at,
            positions.updated_at,
            positions.description,
            positions.advanced_search_status,
            employers.id,
            employers.name,
            employers.created_at,
            employers.updated_at,
            employers.wizard_completed_at,
            employers.hidden_at,
            employers.brand_score
  ) as t;

