SELECT
  t.e_id                                        AS id,
  t.e_name                                      AS name,
  t.e_created_at                                AS created_at,
  t.e_updated_at                                AS updated_at,
  t.e_wizard_completed_at                       AS wizard_completed_at,
  t.e_hidden_at                                 AS hidden_at,
  t.e_brand_score                               AS brand_score,
  array_to_json(t.users)                        AS users,
  array_to_json(t.categories)                   AS categories,
  array_to_json(t.category_functions)           AS category_functions,
  array_to_json(t.employer_types)               AS employer_types,
  array_to_json(t.sectors)                      AS sectors,
  json_build_object(
      'id', t.b_id,
      'credits_available', t.b_credits_available,
      'braintree_customer_id', t.b_braintree_customer_id,
      'created_at', t.b_created_at,
      'updated_at', t.b_updated_at
  )                                             AS billing_account,
  --   billing assocations skipped for now
  array_to_json(t.invitations)                  AS invitations,
  array_to_json(t.employer_locations)           AS employer_locations,
  array_to_json(t.unlocked_matches)             AS unlocked_matches,
  json_build_object(
      'id', t.cp_id,
      'company_name', t.cp_company_name,
      'contact_name', t.cp_contact_name,
      'email', t.cp_email,
      'company_information', t.cp_company_information,
      'created_at', t.cp_created_at,
      'updated_at', t.cp_updated_at,
      'logo', t.cp_logo,
      'key_facts', t.cp_key_facts,
      'currently_hiring', t.cp_currently_hiring,
      'primary_image', t.cp_primary_image,
      'video_embed', t.cp_video_embed,
      'slug', t.cp_slug,
      'industries', t.cp_industries,
      'active', t.cp_active,
      'home_page', t.cp_home_page,
      'level', t.cp_level,
      'testimonials', t.cp_testimonials,
      'banner_image', t.cp_banner_image,
      'banner_image_original_filename', t.cp_banner_image_original_filename
  )                                             AS company_profile,
  array_to_json(t.profile_employer_ratings)     AS profile_employer_ratings,
  array_to_json(t.assisted_search_surveys)      AS assisted_search_surveys,
  array_to_json(t.employers_trade_associations) AS employers_trade_associations,
  array_to_json(t.trade_associations)           AS trade_associations
FROM
  (
    WITH eu AS (
        SELECT
          employers.id                          AS e_id,
          array_agg(users)
            FILTER (WHERE users.id IS NOT NULL) AS users
        FROM employers
          LEFT OUTER JOIN employer_users ON employer_users.employer_id = employers.id
          LEFT OUTER JOIN users ON users.id = employer_users.user_id
        GROUP BY
          employers.id
    ), ec AS (
        SELECT
          employers.id                               AS e_id,
          array_agg(categories)
            FILTER (WHERE categories.id IS NOT NULL) AS categories
        FROM employers
          LEFT OUTER JOIN categories_employers ON categories_employers.employer_id = employers.id
          LEFT OUTER JOIN categories ON categories.id = categories_employers.category_id
        GROUP BY
          employers.id
    ), ecf AS (
        SELECT
          employers.id                                       AS e_id,
          array_agg(category_functions)
            FILTER (WHERE category_functions.id IS NOT NULL) AS category_functions
        FROM employers
          LEFT OUTER JOIN category_functions_employers ON category_functions_employers.employer_id = employers.id
          LEFT OUTER JOIN category_functions
            ON category_functions.id = category_functions_employers.category_function_id
        GROUP BY
          employers.id
    ), eep AS (
        SELECT
          employers.id                                   AS e_id,
          array_agg(employer_types)
            FILTER (WHERE employer_types.id IS NOT NULL) AS employer_types
        FROM employers
          LEFT OUTER JOIN employer_types_employers ON employer_types_employers.employer_id = employers.id
          LEFT OUTER JOIN employer_types ON employer_types.id = employer_types_employers.employer_type_id
        GROUP BY
          employers.id
    ), es AS (
        SELECT
          employers.id                            AS e_id,
          array_agg(sectors)
            FILTER (WHERE sectors.id IS NOT NULL) AS sectors
        FROM employers
          LEFT OUTER JOIN employers_sectors ON employers_sectors.employer_id = employers.id
          LEFT OUTER JOIN sectors ON sectors.id = employers_sectors.sector_id
        GROUP BY
          employers.id
    ), ei AS (
        SELECT
          employers.id                                AS e_id,
          array_agg(invitations)
            FILTER (WHERE invitations.id IS NOT NULL) AS invitations
        FROM employers
          LEFT OUTER JOIN invitations ON invitations.employer_id = employers.id
        GROUP BY
          employers.id
    ), eel AS (
        SELECT
          employers.id                                       AS e_id,
          array_agg(employer_locations)
            FILTER (WHERE employer_locations.id IS NOT NULL) AS employer_locations
        FROM employers
          LEFT OUTER JOIN employer_locations ON employer_locations.employer_id = employers.id
        GROUP BY
          employers.id
    ), eum AS (
        SELECT
          employers.id                                     AS e_id,
          array_agg(unlocked_matches)
            FILTER (WHERE unlocked_matches.id IS NOT NULL) AS unlocked_matches
        FROM employers
          LEFT OUTER JOIN unlocked_matches ON unlocked_matches.employer_id = employers.id
        GROUP BY
          employers.id
    ), eper AS (
        SELECT
          employers.id                                             AS e_id,
          array_agg(profile_employer_ratings)
            FILTER (WHERE profile_employer_ratings.id IS NOT NULL) AS profile_employer_ratings
        FROM employers
          LEFT OUTER JOIN profile_employer_ratings ON profile_employer_ratings.employer_id = employers.id
        GROUP BY
          employers.id
    ), eass AS (
        SELECT
          employers.id                                            AS e_id,
          array_agg(assisted_search_surveys)
            FILTER (WHERE assisted_search_surveys.id IS NOT NULL) AS assisted_search_surveys
        FROM employers
          LEFT OUTER JOIN assisted_search_surveys ON assisted_search_surveys.employer_id = employers.id
        GROUP BY
          employers.id
    ), eta AS (
        SELECT
          employers.id                                                 AS e_id,
          array_agg(employers_trade_associations)
            FILTER (WHERE employers_trade_associations.id IS NOT NULL) AS employers_trade_associations,
          array_agg(trade_associations)
            FILTER (WHERE trade_associations.id IS NOT NULL)           AS trade_associations
        FROM employers
          LEFT OUTER JOIN employers_trade_associations ON employers_trade_associations.employer_id = employers.id
          LEFT OUTER JOIN trade_associations
            ON trade_associations.id = employers_trade_associations.trade_association_id
        GROUP BY
          employers.id
    )
    SELECT
      employers.id                                    AS e_id,
      employers.name                                  AS e_name,
      employers.created_at                            AS e_created_at,
      employers.updated_at                            AS e_updated_at,
      employers.wizard_completed_at                   AS e_wizard_completed_at,
      employers.hidden_at                             AS e_hidden_at,
      employers.brand_score                           AS e_brand_score,
      billing_accounts.id                             AS b_id,
      billing_accounts.credits_available              AS b_credits_available,
      billing_accounts.braintree_customer_id          AS b_braintree_customer_id,
      billing_accounts.created_at                     AS b_created_at,
      billing_accounts.updated_at                     AS b_updated_at,
      company_profiles.id                             AS cp_id,
      company_profiles.company_name                   AS cp_company_name,
      company_profiles.contact_name                   AS cp_contact_name,
      company_profiles.email                          AS cp_email,
      company_profiles.company_information            AS cp_company_information,
      company_profiles.created_at                     AS cp_created_at,
      company_profiles.updated_at                     AS cp_updated_at,
      company_profiles.logo                           AS cp_logo,
      company_profiles.key_facts                      AS cp_key_facts,
      company_profiles.currently_hiring               AS cp_currently_hiring,
      company_profiles.primary_image                  AS cp_primary_image,
      company_profiles.video_embed                    AS cp_video_embed,
      company_profiles.slug                           AS cp_slug,
      company_profiles.industries                     AS cp_industries,
      company_profiles.active                         AS cp_active,
      company_profiles.home_page                      AS cp_home_page,
      company_profiles.level                          AS cp_level,
      company_profiles.testimonials                   AS cp_testimonials,
      company_profiles.banner_image                   AS cp_banner_image,
      company_profiles.banner_image_original_filename AS cp_banner_image_original_filename,
      eu.users                                        AS users,
      ec.categories                                   AS categories,
      ecf.category_functions                          AS category_functions,
      eep.employer_types                              AS employer_types,
      es.sectors                                      AS sectors,
      ei.invitations                                  AS invitations,
      eel.employer_locations                          AS employer_locations,
      eum.unlocked_matches                            AS unlocked_matches,
      eper.profile_employer_ratings                   AS profile_employer_ratings,
      eass.assisted_search_surveys                    AS assisted_search_surveys,
      eta.employers_trade_associations                AS employers_trade_associations,
      eta.trade_associations                          AS trade_associations
    FROM employers
      LEFT OUTER JOIN eu ON employers.id = eu.e_id
      LEFT OUTER JOIN ec ON employers.id = ec.e_id
      LEFT OUTER JOIN ecf ON employers.id = ecf.e_id
      LEFT OUTER JOIN eep ON employers.id = eep.e_id
      LEFT OUTER JOIN es ON employers.id = es.e_id
      LEFT OUTER JOIN ei ON employers.id = ei.e_id
      LEFT OUTER JOIN eel ON employers.id = eel.e_id
      LEFT OUTER JOIN eum ON employers.id = eum.e_id
      LEFT OUTER JOIN eper ON employers.id = eper.e_id
      LEFT OUTER JOIN eass ON employers.id = eass.e_id
      LEFT OUTER JOIN eta ON employers.id = eta.e_id
      LEFT OUTER JOIN billing_accounts ON billing_accounts.employer_id = employers.id
      LEFT OUTER JOIN company_profiles ON company_profiles.employer_id = employers.id
  ) AS t;
