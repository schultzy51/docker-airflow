# Setup

## Mysql Setup

Log in as the `root` user and run the following:
```
CREATE DATABASE IF NOT EXISTS airflow;
CREATE USER 'airflow'@'%' IDENTIFIED BY 'airflow';
GRANT ALL ON airflow.* TO 'airflow'@'%';
```

## Kubernetes Setup

Give the GCE instance for the node a static ip.
Give access to the Cloud SQL instance from that new static ip.
Replace the ip address for the Cloud SQL instance in airflow.cfg and script/entrypoint.sh

## UI

http://23.236.49.235:32080/
